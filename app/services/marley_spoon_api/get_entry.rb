# frozen_string_literal: true

module MarleySpoonApi
  class GetEntry< MarleySpoonApi::Base
    private
      def request
        Faraday.get(url, query_params, auth_header)
      end

      def path
        "#{space_id}/environments/#{environment_id}/entries"
      end

      def query_params
        { 'sys.id': params[:entry_id] }
      end
  end
end
