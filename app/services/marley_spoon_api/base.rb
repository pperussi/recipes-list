
module MarleySpoonApi
  class Base
    def initialize(**params)
      @params = params[:params]
    end

    def call
      response = request

      JSON.parse(response.body).deep_symbolize_keys
    end

    private
      attr_reader :params

      def url
        "#{ENV["MARLEY_SPOON_API_HOST"]}#{path}"
      end

      def auth_header
        { 'Authorization' => "Bearer #{access_token}" }
      end

      def space_id
        ENV["CONTENTFUL_SPACE_ID"]
      end

      def environment_id
        ENV["CONTENTFUL_ENVIRONMENT_ID"]
      end

      def access_token
        ENV["CONTENTFUL_ACCESS_TOKEN"]
      end
  end
end
