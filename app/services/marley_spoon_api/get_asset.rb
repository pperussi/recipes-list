# frozen_string_literal: true

module MarleySpoonApi
  class GetAsset< MarleySpoonApi::Base
    private
      def request
        Faraday.get(url, params = nil, auth_header)
      end

      def path
        "#{space_id}/environments/#{environment_id}/assets/#{params[:asset_id]}"
      end
  end
end
