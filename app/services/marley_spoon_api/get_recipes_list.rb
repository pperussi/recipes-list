# frozen_string_literal: true

module MarleySpoonApi
  class GetRecipesList < MarleySpoonApi::Base
    private
      def request
        Faraday.get(url, query_params, auth_header)
      end

      def path
        "#{space_id}/environments/#{environment_id}/entries"
      end

      def query_params
        { content_type: 'recipe'}
      end
  end
end
