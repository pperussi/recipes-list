# frozen_string_literal: true

module ProcessApiResponse
  class CompleteRecipeInformation
    def initialize(recipe)
      @recipe = recipe
    end

    def call
      return unless recipe.present?

      recipe[:chef] = chef_name
      recipe[:tags] = tags_name

      recipe
    end

    private
      attr_reader :recipe

      def chef_name
        return unless recipe[:chef].present?

        params = { entry_id: recipe[:chef][:sys][:id] }

        MarleySpoonApi::GetEntry.new(params:).call[:items].first[:fields][:name]
      end

      def tags_name
        return unless recipe[:tags].present?

        recipe[:tags].map do |recipe|
          params = { entry_id: recipe[:sys][:id] }

          MarleySpoonApi::GetEntry.new(params:).call[:items].first[:fields][:name]
        end
      end
  end
end
