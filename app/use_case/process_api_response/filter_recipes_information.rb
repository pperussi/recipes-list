# frozen_string_literal: true

module ProcessApiResponse
  class FilterRecipesInformation
    def initialize
    end

    def call
      results = recipes_data

      return if results.nil?

      results.map do |result|
        fields = result[:fields]

        {
          id: result[:sys][:id],
          title: fields[:title],
          description: fields[:description],
          photo: photo_url(fields[:photo][:sys][:id]),
          chef: fields[:chef],
          tags: fields[:tags]
        }
      end
    end

    private
      attr_reader :page

      def recipes_data
        MarleySpoonApi::GetRecipesList.new.call[:items]
      end

      def photo_url(photo_id)
        params = { asset_id: photo_id }

        MarleySpoonApi::GetAsset.new(params:).call[:fields][:file][:url]
      end
  end
end
