# frozen_string_literal: true

class RecipesController < ApplicationController
  def index
    @recipes = ProcessApiResponse::FilterRecipesInformation.new.call

    if @recipes.nil?
      redirect_back fallback_location: root_path
    end
  end

  def show
    @recipe = ProcessApiResponse::CompleteRecipeInformation.new(params[:recipe]).call

    if @recipe.nil?
      flash[:notice] = "Something bad happened. Try again latter."
      redirect_back fallback_location: root_path
    end
  end
end
