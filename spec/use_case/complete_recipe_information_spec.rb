# frozen_string_literal: true

require "rails_helper"

describe ProcessApiResponse::CompleteRecipeInformation do
  subject { described_class.new(recipe) }

  describe "#call" do
    let(:get_entry_response1) { instance_double(MarleySpoonApi::GetEntry, call: chef_response) }
    let(:get_entry_response2) { instance_double(MarleySpoonApi::GetEntry, call: tags_response) }

    context "when process the api response" do
      let(:recipe) do
        {
          id: '2E8bc3VcJmA8OgmQsageas',
          title: 'Some food',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
          photo: 'http://www.somefood.com',
          chef: { sys: { id: '3TJp6aDAcMw6yMiE82Oy0K' } },
          tags: [{ sys: { id: '437eO3ORCME46i02SeCW46' } }]
        }
      end

      let(:chef_response) { { items: [{ fields: { name: 'Super Chef'} } ] } }

      let(:tags_response) { { items: [{ fields: { name: 'Vegan'} }] } }

      let(:result) do
        {
          id: '2E8bc3VcJmA8OgmQsageas',
          title: 'Some food',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
          photo: 'http://www.somefood.com',
          chef: 'Super Chef',
          tags: ['Vegan']
        }
      end

      before do
        allow(MarleySpoonApi::GetEntry).to receive(:new)
                                       .with({:params=>{:entry_id=>"3TJp6aDAcMw6yMiE82Oy0K"}})
                                       .and_return(get_entry_response1)
        allow(MarleySpoonApi::GetEntry).to receive(:new)
                                       .with({:params=>{:entry_id=>"437eO3ORCME46i02SeCW46"}})
                                       .and_return(get_entry_response2)
      end

      it { expect(subject.call).to eq(result) }
    end
  end
end
