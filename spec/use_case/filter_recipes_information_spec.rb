# frozen_string_literal: true

require "rails_helper"

describe ProcessApiResponse::FilterRecipesInformation do
  subject { described_class.new }

  describe "#call" do
    let(:get_recipes_double) { instance_double(MarleySpoonApi::GetRecipesList, call: recipes_response) }
    let(:get_asset_double) { instance_double(MarleySpoonApi::GetAsset, call: asset_response) }

    context "when process the api response" do
      let(:page) { 0 }

      let(:recipes_response) do
        { items: [
          { 
            sys: { id: '2E8bc3VcJmA8OgmQsageas' },
            fields: {
              title: 'Some food',
              photo: { sys: { id: '61XHcqOBFYAYCGsKugoMYK' } },
              description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
              chef: { sys: { id: '3TJp6aDAcMw6yMiE82Oy0K' } },
              tags: [{ sys: { id: '437eO3ORCME46i02SeCW46' } }]
            }
          }
        ] }
      end

      let(:asset_response) do
        { fields: { file: { url: 'http://www.somefood.com'} } }
      end

      let(:result) do
        [
          {
            id: '2E8bc3VcJmA8OgmQsageas',
            title: 'Some food',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
            photo: 'http://www.somefood.com',
            chef: { sys: { id: '3TJp6aDAcMw6yMiE82Oy0K' } },
            tags: [{ sys: { id: '437eO3ORCME46i02SeCW46' } }]
          }
        ]
      end

      before do
        allow(MarleySpoonApi::GetRecipesList).to receive(:new).and_return(get_recipes_double)
        allow(MarleySpoonApi::GetAsset).to receive(:new).and_return(get_asset_double)
      end

      it { expect(subject.call).to eq(result) }
    end
  end
end
