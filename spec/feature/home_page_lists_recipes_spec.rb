# frozen_string_literal: true

require "rails_helper"

describe "User see a list of recipes", type: :feature do
  let(:result) do
    [
      {
        id: '2E8bc3VcJmA8OgmQsageas',
        title: 'Some food',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
        photo: 'http://www.somefood.com',
        chef: { sys: { id: '3TJp6aDAcMw6yMiE82Oy0K' } },
        tags: [{ sys: { id: '437eO3ORCME46i02SeCW46' } }]
      }
    ]
  end

  let(:recipes_list) { instance_double(ProcessApiResponse::FilterRecipesInformation, call: result) }
  
  before do
    allow(ProcessApiResponse::FilterRecipesInformation).to receive(:new).and_return(recipes_list)
  end

  scenario "successfuly" do
    visit root_path

    expect(page).to have_content(result.first[:title])
  end
end
