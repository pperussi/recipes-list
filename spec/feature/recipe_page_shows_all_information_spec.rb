# frozen_string_literal: true

require "rails_helper"

describe "User see a recipe complete information", type: :feature do
  let(:recipes_list) do
    [
      {
        id: '2E8bc3VcJmA8OgmQsageas',
        title: 'Some food',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
        photo: 'http://www.somefood.com',
        chef: { sys: { id: '3TJp6aDAcMw6yMiE82Oy0K' } },
        tags: [{ sys: { id: '437eO3ORCME46i02SeCW46' } }]
      }
    ]
  end

  let(:recipe_info) do
    {
      id: '2E8bc3VcJmA8OgmQsageas',
      title: 'Some food',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
      photo: 'https://picsum.photos/id/237/200/300',
      chef: 'Super Chef',
      tags: ['Vegan']
    }
  end

  let(:filter_recipes_double) { instance_double(ProcessApiResponse::FilterRecipesInformation, call: recipes_list) }
  let(:complete_recipe_double) { instance_double(ProcessApiResponse::CompleteRecipeInformation, call: recipe_info) }
  
  before do
    allow(ProcessApiResponse::FilterRecipesInformation).to receive(:new).and_return(filter_recipes_double)
    allow(ProcessApiResponse::CompleteRecipeInformation).to receive(:new).and_return(complete_recipe_double)
  end

  scenario "successfuly" do
    visit root_path

    click_on "See more"

    expect(page).to have_content(recipe_info[:description])
    expect(page).to have_content(recipe_info[:chef])
    expect(page).to have_content(recipe_info[:tags][0])
  end
end
