# frozen_string_literal: true

require "rails_helper"

describe MarleySpoonApi::GetAsset, :vcr do
  subject { described_class.new(params:) }

  describe "#call" do
    context "when the base request is made" do
      let(:params) { { asset_id: '5mFyTozvSoyE0Mqseoos86' } }
      let(:response) { subject.call }

      it { expect(response[:fields].present?).to be true }
    end
  end
end
