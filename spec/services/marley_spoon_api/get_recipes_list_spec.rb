# frozen_string_literal: true

require "rails_helper"

describe MarleySpoonApi::GetRecipesList, :vcr do
  subject { described_class.new(params:) }

  describe "#call" do
    context "when the base request is made" do
      let(:params) { {} }
      let(:response) { subject.call }

      it { expect(response[:items].present?).to be true }
    end
  end
end
