# frozen_string_literal: true

require "rails_helper"

describe MarleySpoonApi::GetEntry, :vcr do
  subject { described_class.new(params:) }

  describe "#call" do
    context "when the base request is made" do
      let(:params) { { entry_id: 'NysGB8obcaQWmq0aQ6qkC' } }
      let(:response) { subject.call }

      it { expect(response[:items].present?).to be true }
    end
  end
end
