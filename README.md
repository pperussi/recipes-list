# Recipes List

This project is a technical challenge proposed by Marley Spoon in its selection process for a backend position, in which it was asked to write a small application to consume the data from an API and display it.

## Getting started

Seting up local development:

- The application can be executed with [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/)

- To be able to make the request the sample has to be removed of `.env.sample` file and is necessary to add the access token from [Contentful](https://www.contentful.com/) API.

Start the application with the command:

```bash
docker-compose up -d
```

The application is acessible on:

```bash
http://localhost:3000
```

To run the tests, while the application is running, run:

```bash
docker-compose exec web rspec
```
